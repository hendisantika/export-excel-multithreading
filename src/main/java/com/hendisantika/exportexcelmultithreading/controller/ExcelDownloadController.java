package com.hendisantika.exportexcelmultithreading.controller;

import com.hendisantika.exportexcelmultithreading.repository.UserRepository;
import com.hendisantika.exportexcelmultithreading.service.ExportExcelService;
import com.hendisantika.exportexcelmultithreading.service.QueryExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by IntelliJ IDEA.
 * Project : export-excel-multithreading
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-02
 * Time: 05:47
 * To change this template use File | Settings | File Templates.
 */
@RestController
public class ExcelDownloadController {
    Logger logger = LoggerFactory.getLogger(ExcelDownloadController.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExportExcelService exportExcelService;

    @Autowired
    private JdbcTemplate template;

    @GetMapping("/getUsers")
    public ResponseEntity<Object> getUsers(HttpServletResponse response) throws Exception {
        exportExcelService.generateExcel(response);
        QueryExecutor queryExecuator = null;
        ExecutorService service = Executors.newFixedThreadPool(10);
        List<List<Map<String, Object>>> recordList = new CopyOnWriteArrayList<>();
        long startTime = System.currentTimeMillis();
        logger.info("Start time : {}", startTime);
        for (int i = 0; i <= userRepository.getCount(); i = i + 10000) {
            queryExecuator = new QueryExecutor(i, i + 10000, template);
            Future<List<Map<String, Object>>> future = service.submit(queryExecuator);
            recordList.add(future.get());
        }
        long endTime = System.currentTimeMillis();
        logger.info("End time : {}", endTime);
        logger.info("Total time : {}", ((endTime - startTime) / 100));
        service.shutdown();
        return new ResponseEntity<>(recordList, HttpStatus.OK);
    }

    @GetMapping("/getRecordCount")
    public int getRecordCount() {
        return userRepository.getCount();
    }

    @GetMapping("/getUser")
    public ResponseEntity<Object> getUser() {
        return new ResponseEntity<>(userRepository.getUsers(), HttpStatus.OK);
    }
}
