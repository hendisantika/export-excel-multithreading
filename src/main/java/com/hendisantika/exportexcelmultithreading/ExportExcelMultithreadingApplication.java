package com.hendisantika.exportexcelmultithreading;

import com.hendisantika.exportexcelmultithreading.model.User;
import com.hendisantika.exportexcelmultithreading.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static java.util.Arrays.asList;

@Log4j2
@SpringBootApplication
public class ExportExcelMultithreadingApplication implements CommandLineRunner {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(ExportExcelMultithreadingApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        log.info("Creating tables");

        jdbcTemplate.execute("DROP TABLE IF EXISTS user ");
        jdbcTemplate.execute(
                "CREATE TABLE user (id SERIAL NOT NULL PRIMARY KEY,"
                        + "  name    varchar(75),"
                        + "  address varchar(75))");

        List<User> userList = asList(
                new User("Uzumaki Naruto", "Konohagakure"),
                new User("Uzumaki Kushina", "Konohagakure"),
                new User("Uchiha Sasuke", "Konohagakure"),
                new User("Hatake Kakashi", "Konohagakure"),
                new User("Namikaze Minato", "Konohagakure"),
                new User("Aburame Shino", "Konohagakure")
        );

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        for (User user : userList) {
            userRepository.create(user);
        }

        // List all users
        userRepository.findAll();

    }
}
