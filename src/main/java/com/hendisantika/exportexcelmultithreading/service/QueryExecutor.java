package com.hendisantika.exportexcelmultithreading.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by IntelliJ IDEA.
 * Project : export-excel-multithreading
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-02
 * Time: 05:46
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
public class QueryExecutor implements Callable<List<Map<String, Object>>> {
    int startIndex;
    int endIndex;
    JdbcTemplate template;

    public QueryExecutor(int startIndex, int endIndex, JdbcTemplate template) {
        super();
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.template = template;
    }

    @Override
    public List<Map<String, Object>> call() throws Exception {
        List<Map<String, Object>> results = null;
        System.out.println(template);
        String query = "SELECT * FROM User LIMIT " + startIndex + "," + endIndex;
        System.out.println(query);
        results = template.queryForList(query);

        return results;
    }

}