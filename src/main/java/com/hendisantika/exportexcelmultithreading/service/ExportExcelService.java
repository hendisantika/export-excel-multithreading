package com.hendisantika.exportexcelmultithreading.service;

import com.hendisantika.exportexcelmultithreading.repository.UserRepository;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by IntelliJ IDEA.
 * Project : export-excel-multithreading
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-02
 * Time: 05:45
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ExportExcelService {
    @Autowired
    private UserRepository userRepository;

    public void generateExcel(HttpServletResponse response) throws Exception {
        Map<Integer, Map<String, Object>> empinfo = new TreeMap<>();

        // Set<String> excelHeaders = new LinkedHashSet<>();
        int count = 0;
        for (Map<String, Object> recordList : userRepository.getUsers()) {
            count++;
            empinfo.put(count, recordList);
        }
        // Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        // Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet(" Employee Info ");

        // Create row object
        XSSFRow row;

        // Iterate over data and write to sheet
        Set<Integer> keyid = empinfo.keySet();
        int rowid = 0;

        for (int key : keyid) {
            row = spreadsheet.createRow(rowid++);
            Map<String, Object> records = empinfo.get(key);
            int cellid = 0;

            for (String keySet : records.keySet()) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue(String.valueOf(records.get(keySet)));
            }
        }

        // Write the workbook in file system

        System.out.println("Writesheet.xlsx written successfully");
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=" + "batch" + ".xlsx");
        response.setHeader("Access-Control-Expose-Headers", "X-ExcelFilename");
        response.setHeader("X-ExcelFilename", "batch" + ".xlsx");
        // write workbook to outputstream
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);
        out.flush();
        out.close();

    }
}
