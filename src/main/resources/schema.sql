create table user (
  id      int auto_increment not null primary key,
  name    varchar(50),
  address varchar(75)
);